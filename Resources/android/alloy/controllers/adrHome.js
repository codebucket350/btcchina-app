function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "adrHome";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.winHome = Ti.UI.createView({
        backgroundColor: Alloy.Globals.ThemeStyles.win.backgroundColor,
        id: "winHome"
    });
    $.__views.winHome && $.addTopLevelView($.__views.winHome);
    $.__views.mainWebView = Ti.UI.createWebView({
        height: "auto",
        id: "mainWebView",
        url: "https://vip.btcchina.com/index/login"
    });
    $.__views.winHome.add($.__views.mainWebView);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    var that = this;
    var headerBar = Alloy.createController("adrHeaderBar", {
        parentView: $.winHome,
        title: args.menuItem.title,
        isFlyout: true
    }).getView();
    var rightMenuButton = Alloy.createController("adrRightMenuButton", {
        parentView: $.winHome,
        context: that
    }).getView();
    headerBar.add(rightMenuButton);
    $.winHome.add(headerBar);
    $.mainWebView.setTop(headerBar.height);
    Ti.App.addEventListener("setUrl", function(e) {
        $.mainWebView.setUrl(e.url);
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;