function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "adrNewsRow";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.row = Ti.UI.createTableViewRow({
        id: "row"
    });
    $.__views.row && $.addTopLevelView($.__views.row);
    $.__views.imgNews = Ti.UI.createImageView({
        id: "imgNews"
    });
    $.__views.row.add($.__views.imgNews);
    $.__views.outerContainer = Ti.UI.createView({
        id: "outerContainer"
    });
    $.__views.row.add($.__views.outerContainer);
    $.__views.lblName = Ti.UI.createLabel({
        id: "lblName"
    });
    $.__views.outerContainer.add($.__views.lblName);
    $.__views.tagView = Ti.UI.createView({
        id: "tagView"
    });
    $.__views.outerContainer.add($.__views.tagView);
    $.__views.lblDetail = Ti.UI.createLabel({
        id: "lblDetail"
    });
    $.__views.outerContainer.add($.__views.lblDetail);
    exports.destroy = function() {};
    _.extend($, $.__views);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;