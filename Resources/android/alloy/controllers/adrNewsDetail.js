function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "adrNewsDetail";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.winDetail = Ti.UI.createWindow({
        id: "winDetail"
    });
    $.__views.winDetail && $.addTopLevelView($.__views.winDetail);
    var __alloyId1 = [];
    $.__views.rowDetail = Ti.UI.createTableViewRow({
        id: "rowDetail"
    });
    __alloyId1.push($.__views.rowDetail);
    $.__views.coverImage = Ti.UI.createImageView({
        id: "coverImage"
    });
    $.__views.rowDetail.add($.__views.coverImage);
    $.__views.lblName = Ti.UI.createLabel({
        id: "lblName"
    });
    $.__views.rowDetail.add($.__views.lblName);
    $.__views.tagView = Ti.UI.createView({
        id: "tagView"
    });
    $.__views.rowDetail.add($.__views.tagView);
    $.__views.lblDetail = Ti.UI.createLabel({
        id: "lblDetail"
    });
    $.__views.rowDetail.add($.__views.lblDetail);
    $.__views.detailTable = Ti.UI.createTableView({
        data: __alloyId1,
        id: "detailTable"
    });
    $.__views.winDetail.add($.__views.detailTable);
    exports.destroy = function() {};
    _.extend($, $.__views);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;