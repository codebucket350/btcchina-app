function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "adrCustomProductView";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.outerView = Ti.UI.createView({
        id: "outerView"
    });
    $.__views.outerView && $.addTopLevelView($.__views.outerView);
    $.__views.innerView = Ti.UI.createView({
        id: "innerView"
    });
    $.__views.outerView.add($.__views.innerView);
    $.__views.imgProduct = Ti.UI.createImageView({
        id: "imgProduct"
    });
    $.__views.innerView.add($.__views.imgProduct);
    $.__views.infoView = Ti.UI.createView({
        id: "infoView"
    });
    $.__views.innerView.add($.__views.infoView);
    $.__views.lblTitle = Ti.UI.createLabel({
        id: "lblTitle"
    });
    $.__views.infoView.add($.__views.lblTitle);
    $.__views.tagView = Ti.UI.createView({
        id: "tagView"
    });
    $.__views.infoView.add($.__views.tagView);
    $.__views.lblDetail = Ti.UI.createLabel({
        id: "lblDetail"
    });
    $.__views.infoView.add($.__views.lblDetail);
    exports.destroy = function() {};
    _.extend($, $.__views);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;