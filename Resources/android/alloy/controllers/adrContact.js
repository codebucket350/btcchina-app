function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "adrContact";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.winContact = Ti.UI.createView({
        id: "winContact"
    });
    $.__views.winContact && $.addTopLevelView($.__views.winContact);
    $.__views.contactScroll = Ti.UI.createScrollView({
        id: "contactScroll"
    });
    $.__views.winContact.add($.__views.contactScroll);
    $.__views.row = Ti.UI.createView({
        id: "row"
    });
    $.__views.contactScroll.add($.__views.row);
    $.__views.tfname = Ti.UI.createTextField({
        id: "tfname"
    });
    $.__views.row.add($.__views.tfname);
    $.__views.row = Ti.UI.createView({
        id: "row"
    });
    $.__views.contactScroll.add($.__views.row);
    $.__views.tfEmail = Ti.UI.createTextField({
        id: "tfEmail"
    });
    $.__views.row.add($.__views.tfEmail);
    $.__views.row = Ti.UI.createView({
        id: "row"
    });
    $.__views.contactScroll.add($.__views.row);
    $.__views.tfSubject = Ti.UI.createTextField({
        id: "tfSubject"
    });
    $.__views.row.add($.__views.tfSubject);
    $.__views.row = Ti.UI.createView({
        id: "row"
    });
    $.__views.contactScroll.add($.__views.row);
    $.__views.taMessage = Ti.UI.createTextArea({
        id: "taMessage"
    });
    $.__views.row.add($.__views.taMessage);
    $.__views.row = Ti.UI.createView({
        id: "row"
    });
    $.__views.contactScroll.add($.__views.row);
    $.__views.btnSendMessage = Ti.UI.createView({
        id: "btnSendMessage"
    });
    $.__views.row.add($.__views.btnSendMessage);
    $.__views.lblSendMessage = Ti.UI.createLabel({
        id: "lblSendMessage"
    });
    $.__views.btnSendMessage.add($.__views.lblSendMessage);
    exports.destroy = function() {};
    _.extend($, $.__views);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;