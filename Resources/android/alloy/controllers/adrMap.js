function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "adrMap";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.winMap = Ti.UI.createView({
        id: "winMap"
    });
    $.__views.winMap && $.addTopLevelView($.__views.winMap);
    var __alloyId0 = [];
    $.__views.mapView = Ti.Map.createView({
        annotations: __alloyId0,
        id: "mapView"
    });
    $.__views.winMap.add($.__views.mapView);
    report ? $.__views.mapView.addEventListener("click", report) : __defers["$.__views.mapView!click!report"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    __defers["$.__views.mapView!click!report"] && $.__views.mapView.addEventListener("click", report);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;