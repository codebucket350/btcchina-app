function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "adrCustomHomeButton";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.btn_custom = Ti.UI.createView({
        id: "btn_custom"
    });
    $.__views.btn_custom && $.addTopLevelView($.__views.btn_custom);
    $.__views.icon = Ti.UI.createImageView({
        id: "icon"
    });
    $.__views.btn_custom.add($.__views.icon);
    $.__views.lbl_title = Ti.UI.createLabel({
        id: "lbl_title"
    });
    $.__views.btn_custom.add($.__views.lbl_title);
    exports.destroy = function() {};
    _.extend($, $.__views);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;