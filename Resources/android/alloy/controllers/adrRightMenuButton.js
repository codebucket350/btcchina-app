function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "adrRightMenuButton";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.btnView = Ti.UI.createView({
        height: "48dp",
        width: "49dp",
        backgroundColor: "transparent",
        right: 0,
        id: "btnView"
    });
    $.__views.btnView && $.addTopLevelView($.__views.btnView);
    $.__views.rightMenuButton = Ti.UI.createButton({
        title: "",
        backgroundImage: "/images/man-icon.png",
        height: "24dp",
        width: "28dp",
        zIndex: 5,
        visible: true,
        id: "rightMenuButton"
    });
    $.__views.btnView.add($.__views.rightMenuButton);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    var that = this;
    this.rightMenuView = args.parentView;
    this.lastWinContext = args.context;
    this.parentView = args.parentView;
    $.btnView.addEventListener("click", function() {
        if (Alloy.Globals.menuVisible) {
            Alloy.Globals.menuVisible = false;
            var animation = Titanium.UI.createAnimation();
            animation.right = 0;
            animation.left = 0;
            animation.duration = 150;
            that.parentView.animate(animation);
        } else {
            Alloy.Globals.menuVisible = true;
            var animation = Titanium.UI.createAnimation();
            animation.right = "40%";
            animation.left = "-40%";
            animation.duration = 150;
            that.parentView.animate(animation);
        }
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;