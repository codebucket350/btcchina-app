function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "adrNews";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.winNews = Ti.UI.createView({
        id: "winNews"
    });
    $.__views.winNews && $.addTopLevelView($.__views.winNews);
    $.__views.newsTable = Ti.UI.createTableView({
        id: "newsTable"
    });
    $.__views.winNews.add($.__views.newsTable);
    $.__views.ind = Ti.UI.createActivityIndicator({
        id: "ind"
    });
    $.__views.winNews.add($.__views.ind);
    exports.destroy = function() {};
    _.extend($, $.__views);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;