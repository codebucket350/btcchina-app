function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "adrProductsView";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.winProduct = Ti.UI.createView({
        id: "winProduct"
    });
    $.__views.winProduct && $.addTopLevelView($.__views.winProduct);
    $.__views.productScroll = Ti.UI.createScrollView({
        id: "productScroll"
    });
    $.__views.winProduct.add($.__views.productScroll);
    $.__views.scrollViewContainer = Ti.UI.createView({
        id: "scrollViewContainer"
    });
    $.__views.productScroll.add($.__views.scrollViewContainer);
    $.__views.ind = Ti.UI.createActivityIndicator({
        id: "ind"
    });
    $.__views.winProduct.add($.__views.ind);
    exports.destroy = function() {};
    _.extend($, $.__views);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;