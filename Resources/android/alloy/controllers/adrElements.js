function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "adrElements";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.winElements = Ti.UI.createView({
        id: "winElements"
    });
    $.__views.winElements && $.addTopLevelView($.__views.winElements);
    $.__views.elementsScroll = Ti.UI.createScrollView({
        id: "elementsScroll"
    });
    $.__views.winElements.add($.__views.elementsScroll);
    $.__views.rowText = Ti.UI.createView({
        id: "rowText"
    });
    $.__views.elementsScroll.add($.__views.rowText);
    $.__views.textFname = Ti.UI.createTextField({
        id: "textFname"
    });
    $.__views.rowText.add($.__views.textFname);
    $.__views.rowSlider = Ti.UI.createView({
        id: "rowSlider"
    });
    $.__views.elementsScroll.add($.__views.rowSlider);
    $.__views.customSlider = Ti.UI.createSlider({
        id: "customSlider"
    });
    $.__views.rowSlider.add($.__views.customSlider);
    $.__views.pBar = Ti.UI.createView({
        id: "pBar"
    });
    $.__views.rowSlider.add($.__views.pBar);
    $.__views.progressView = Ti.UI.createView({
        id: "progressView"
    });
    $.__views.rowSlider.add($.__views.progressView);
    $.__views.lblProgress = Ti.UI.createLabel({
        id: "lblProgress"
    });
    $.__views.progressView.add($.__views.lblProgress);
    $.__views.rowSwitch = Ti.UI.createView({
        id: "rowSwitch"
    });
    $.__views.elementsScroll.add($.__views.rowSwitch);
    $.__views.basicSwitch1 = Ti.UI.createSwitch({
        id: "basicSwitch1"
    });
    $.__views.rowSwitch.add($.__views.basicSwitch1);
    $.__views.switchBlack = Ti.UI.createButton({
        id: "switchBlack"
    });
    $.__views.rowSwitch.add($.__views.switchBlack);
    $.__views.switchGreenRed = Ti.UI.createButton({
        id: "switchGreenRed"
    });
    $.__views.rowSwitch.add($.__views.switchGreenRed);
    $.__views.rowButtonMedium = Ti.UI.createView({
        id: "rowButtonMedium"
    });
    $.__views.elementsScroll.add($.__views.rowButtonMedium);
    $.__views.btnSmall1 = Ti.UI.createButton({
        id: "btnSmall1"
    });
    $.__views.rowButtonMedium.add($.__views.btnSmall1);
    $.__views.btnSmall2 = Ti.UI.createButton({
        id: "btnSmall2"
    });
    $.__views.rowButtonMedium.add($.__views.btnSmall2);
    $.__views.rowButtonLarge = Ti.UI.createView({
        id: "rowButtonLarge"
    });
    $.__views.elementsScroll.add($.__views.rowButtonLarge);
    $.__views.btnLarge = Ti.UI.createButton({
        id: "btnLarge"
    });
    $.__views.rowButtonLarge.add($.__views.btnLarge);
    exports.destroy = function() {};
    _.extend($, $.__views);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;