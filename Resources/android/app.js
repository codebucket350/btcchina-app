var Alloy = require("alloy"), _ = Alloy._, Backbone = Alloy.Backbone;

Alloy.Globals.isIOS7 = function() {
    var version = Titanium.Platform.version.split(".");
    return version[0];
};

Alloy.Globals.dp = Titanium.Platform.Android ? Ti.Platform.displayCaps.dpi / 160 : 1;

Alloy.Globals.menuVisible = false;

Alloy.Globals.URLS = {
    news_url: "http://skounis.s3.amazonaws.com/mobile-apps/barebone/news.json",
    products_url: "http://skounis.s3.amazonaws.com/mobile-apps/barebone/products.json"
};

Alloy.Globals.navWindows = [];

Alloy.Globals.animations = {
    left: {
        left: 275,
        curve: Ti.UI.ANIMATION_CURVE_EASE_OUT,
        duration: 200
    },
    right: {
        left: 0,
        curve: Ti.UI.ANIMATION_CURVE_EASE_OUT,
        duration: 200
    },
    slide_out_top: Titanium.UI.createAnimation({
        top: Ti.Platform.Android ? "-320dp" : -320
    }),
    slide_in_top: Titanium.UI.createAnimation({
        top: Ti.Platform.Android ? "48dp" : 0
    })
};

Alloy.Globals.ThemeStyles = {
    navTintColor: "#fff",
    win: {
        backgroundColor: "#232323",
        barColor: "#232323",
        separatorColor: "#343434",
        navTintColor: "#fff"
    },
    flyout_menu: {
        backgroundColor: "#292929"
    },
    flyout_menu_item: {
        font: {
            fontSize: "18dp",
            fontFamily: "Montserrat",
            fontWeight: "Regular"
        },
        rowHeight: 60 * Alloy.Globals.dp,
        selectedBackgroundColor: "#8c5e7a",
        verticalDividerColor: "#343434",
        rowSeparatorColor: "#343434"
    },
    right_menu: {
        color: "#656565",
        backgroundColor: "#292929",
        selectedBackgroundColor: "#8c5e7a",
        rowSeparatorColor: "#343434",
        font: {
            fontSize: "18dp",
            fontFamily: "Montserrat",
            fontWeight: "Regular"
        },
        width: 175,
        rowHeight: 48
    }
};

Alloy.Globals.FlyoutMenu = [ {
    title: "",
    name: "_main_menu",
    controller: "",
    color: "#f6f6f6",
    icon: "/images/btcchina-black-logo.png",
    iconAndroid: "/images/btcchina-black-logo.png",
    rowBackgroundColor: "#292929",
    isHeader: true
}, {
    title: "Exchange Home",
    name: "_exchange_home",
    controller: Ti.Platform.Android ? "adrHome" : "Home",
    color: "#656565",
    icon: "/images/ic_home.png",
    iconAndroid: "/images/ic_home.png",
    rowBackgroundColor: "#292929"
}, {
    title: "User Settings",
    name: "_user_settings",
    controller: Titanium.Platform.Android ? "adrNews" : "News",
    url: "https://vip.btcchina.com/security/twofactor",
    color: "#656565",
    icon: "/images/ic_settings.png",
    iconAndroid: "/images/ic_settings.png",
    rowBackgroundColor: "#292929"
}, {
    title: "Security Center",
    name: "_security",
    url: "https://vip.btcchina.com/security",
    controller: Titanium.Platform.Android ? "adrProducts" : "Products",
    color: "#656565",
    icon: "/images/ic_products.png",
    iconAndroid: "/images/ic_products.png",
    rowBackgroundColor: "#292929"
}, {
    title: "Feedback",
    name: "_feedback",
    url: "https://w.btcchina.com",
    controller: Titanium.Platform.Android ? "adrMap" : "Map",
    color: "#656565",
    icon: "/images/ic_contact.png",
    iconAndroid: "/images/ic_contact.png",
    rowBackgroundColor: "#292929"
}, {
    title: "Logout",
    name: "_logout",
    controller: Titanium.Platform.Android ? "adrContact" : "Contact",
    url: "https://vip.btcchina.com/index/logout",
    color: "#656565",
    icon: "/images/ic_elements.png",
    iconAndroid: "/images/ic_elements.png",
    rowBackgroundColor: "#292929"
} ];

Alloy.Globals.FlyoutMenuRight = [ {
    title: "Balances",
    name: "_main_menu_right",
    position: "right",
    controller: "",
    color: "#f6f6f6",
    rowBackgroundColor: "#292929",
    isHeader: true
}, {
    title: "CNY",
    name: "_cny",
    position: "right",
    controller: Ti.Platform.Android ? "adrHome" : "Home",
    color: "#656565",
    icon: "/images/ic_products.png",
    iconAndroid: "/images/ic_products.png",
    rowBackgroundColor: "#292929"
}, {
    title: "BTC",
    name: "_btc",
    position: "right",
    controller: Titanium.Platform.Android ? "adrNews" : "News",
    color: "#656565",
    icon: "/images/ic_products.png",
    iconAndroid: "/images/ic_products.png",
    rowBackgroundColor: "#292929"
}, {
    title: "LTC",
    name: "_ltc",
    position: "right",
    controller: Titanium.Platform.Android ? "adrProducts" : "Products",
    color: "#656565",
    icon: "/images/ic_products.png",
    iconAndroid: "/images/ic_products.png",
    rowBackgroundColor: "#292929"
}, {
    title: "Total assets",
    name: "_total_assets",
    position: "right",
    controller: Titanium.Platform.Android ? "adrProducts" : "Products",
    color: "#656565",
    icon: "/images/ic_products.png",
    iconAndroid: "/images/ic_products.png",
    rowBackgroundColor: "#292929"
} ];

Alloy.Globals.rightMenuItems = [ {
    title: "Account Info",
    color: "#656565"
}, {
    title: "CNY",
    color: "#656565"
}, {
    title: "BTC",
    color: "#656565"
}, {
    title: "LTC",
    color: "#656565"
} ];

Alloy.createController("index");