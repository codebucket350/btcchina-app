var that = this;
this.win2open = null;
this.selectedIndex = 0;

$.winParent.addEventListener('android:back', function(e) {
	if (Alloy.Globals.navWindows.length == 0) {
		$.winParent.close();
	} else if (Alloy.Globals.navWindows.length > 0) {
		var length = Alloy.Globals.navWindows.length;
		for (var i = 0; i < length; i++) {
			Alloy.Globals.navWindows.pop();
		};
	}
});

// populate the left and right flyout tables on opening index (exchange home) page
$.winParent.addEventListener('open', function(e) {
	/*
	 * Left flyoutTable Menu rows will be created here and populated
	 */
	var rows = [];
	_.each(Alloy.Globals.FlyoutMenu, function(item) {
		rows.push(Alloy.createController("adrFlyoutRow", {
			image : item.iconAndroid,
			title : item.title,
			name : item.name,
			controller : item.controller,
			menuItem : item,
		}).getView());
	});
	$.flyoutTable.setData(rows);

	$.flyoutTable.addEventListener('menu:selected', function(e) {
		if (e.menuItem.name == '_main_menu') {
			// DO NOTHING
		} else if (e.menuItem.name == '_options') {
			alert('Option Clicked');
		} else if (e.menuItem.name == '_exchange_home') {
			var last_opened_window = null;
			if (that.win2open) {
				last_opened_window = that.win2open;
				that.win2open = null;
			}
			var win_path = undefined;
			win_path = e.menuItem.controller;
			var NewWindow = Alloy.createController(win_path, {
				menuItem : e.menuItem,
				isFlyout : true
			}).getView();

			that.win2open = NewWindow;
			that.win2open.zIndex = 5;
			$.winParent.add(that.win2open);

			if (last_opened_window) {
				setTimeout(function(e) {
					$.winParent.remove(last_opened_window);
				}, 1000);
			}

			// Add window source to Stack
			Alloy.Globals.navWindows.push({
				src : e.menuItem.controller
			});
			last_window = e.menuItem.controller;
		} else {
			//Ti.API.info(JSON.stringify(that.win2open));
			Ti.App.fireEvent('setUrl',{url : e.menuItem.url});
			headerBarTitle = (e.menuItem.title == 'Logout') ? 'Exchange Home' : e.menuItem.title; 
			Ti.App.fireEvent('setHeaderBarTitle',{headerBarTitle : headerBarTitle});
		} 
	});
	
	Ti.App.addEventListener('hideFlyoutTable', function(e) {
		setTimeout(function(e) {
			$.flyoutTable.hide();
		    },500);
	});

	Ti.App.addEventListener('showFlyoutTable', function(e) {
		$.flyoutTable.show();
	});

	// MENU SELECTION FIRED FOR THE FIRST TIME MANUALLY
	// TO OPEN THE FIRST VIEW IN THE MENU LIST.
	// FOR EXAMPLE HOME IS OUR FIRST VIEW IN MENU
	$.flyoutTable.fireEvent('menu:selected', {
		menuItem : Alloy.Globals.FlyoutMenu[1]
	});

	$.flyoutTable.addEventListener('click', function(obj) {
		if (that.selectedIndex != obj.index) {
			$.flyoutTable.fireEvent('hide', {});
			that.selectedIndex = obj.index;
			$.flyoutTable.fireEvent('menu:selected', {
				menuItem : Alloy.Globals.FlyoutMenu[obj.index]
			});

		} else {
			$.flyoutTable.fireEvent('hide', {});
		}
	});

	$.flyoutTable.addEventListener('hide', function(e) {
		var animation = Titanium.UI.createAnimation();
		animation.left = 0;
		animation.right = 0;
		animation.duration = 150;
		that.win2open.animate(animation);
		Alloy.Globals.menuVisible = false;
	});
	
	//** RIGHT flyoutTable **//
	var rows = [];
	_.each(Alloy.Globals.FlyoutMenuRight, function(item) {
		rows.push(Alloy.createController("adrFlyoutRow", {
			image : item.iconAndroid,
			title : item.title,
			name : item.name,
			controller : item.controller,
			menuItem : item
		}).getView());
	});
	$.flyoutTableRight.setData(rows);

	/*$.flyoutTable.addEventListener('menu:selected', function(e) {
		if (e.menuItem.name == '_main_menu') {
			// DO NOTHING
		} else if (e.menuItem.name == '_options') {
			alert('Option Clicked');
		} else {
			var last_opened_window = null;
			if (that.win2open) {
				last_opened_window = that.win2open;
				that.win2open = null;
			}
			var win_path = undefined;
			win_path = e.menuItem.controller;
			var NewWindow = Alloy.createController(win_path, {
				menuItem : e.menuItem,
				isFlyout : true
			}).getView();

			that.win2open = NewWindow;
			that.win2open.zIndex = 5;
			$.winParent.add(that.win2open);

			if (last_opened_window) {
				setTimeout(function(e) {
					$.winParent.remove(last_opened_window);
				}, 1000);
			}

			// Add window source to Stack
			Alloy.Globals.navWindows.push({
				src : e.menuItem.controller
			});
			last_window = e.menuItem.controller;
		}
	});*/


	$.flyoutTableRight.addEventListener('click', function(obj) {
		if (that.selectedIndex != obj.index) {
			$.flyoutTableRight.fireEvent('hide', {});
			that.selectedIndex = obj.index;
			/*$.flyoutTableRight.fireEvent('menu:selected', {
				menuItem : Alloy.Globals.FlyoutMenu[obj.index]
			});*/

		} else {
			$.flyoutTableRight.fireEvent('hide', {});
		}
	});

	$.flyoutTableRight.addEventListener('hide', function(e) {
		var animation = Titanium.UI.createAnimation();
		animation.right = 0;
		animation.left = 0;
		animation.duration = 150;
		that.win2open.animate(animation);
		Alloy.Globals.menuVisible = false;
	});

	 
});

$.winParent.open();
