var args = arguments[0] || {};
var that = this;

// REQUIRE THE HEADER BAR FROM headerBar controller
// we will pass our Home View so we can animate it when we click the menu button
var headerBar = Alloy.createController("adrHeaderBar", {
	parentView : $.winHome,
	title : args.menuItem.title,
	isFlyout : true
}).getView();

// Setup Flyout right menu button
var rightMenuButton = Alloy.createController("adrRightMenuButton", {
	//parentView : this.rightMenuView,
	parentView : $.winHome,
	context : that
}).getView();
headerBar.add(rightMenuButton);

$.winHome.add(headerBar);

$.mainWebView.setTop(headerBar.height);

Ti.App.addEventListener('setUrl', function(e) {
	$.mainWebView.setUrl(e.url);
});
