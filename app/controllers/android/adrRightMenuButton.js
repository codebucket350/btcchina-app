var args = arguments[0] || {};
var that = this;
this.rightMenuView = args.parentView;
this.lastWinContext = args.context;
this.parentView = args.parentView;
/*$.btnView.addEventListener('click', function(e) {
	if (!that.lastWinContext.isRightMenuShown) {
		that.rightMenuView.animate(Alloy.Globals.animations.slide_in_top);
		that.lastWinContext.isRightMenuShown = true;
	} else {
		that.lastWinContext.isRightMenuShown = false;
		that.rightMenuView.animate(Alloy.Globals.animations.slide_out_top);
	}
});*/

$.btnView.addEventListener('click', function(e) {
	if (Alloy.Globals.menuVisible) {
		Alloy.Globals.menuVisible = false;
		var animation = Titanium.UI.createAnimation();
		animation.right = 0;
		animation.left = 0;
		animation.duration = 150;
		that.parentView.animate(animation);
	} else {
		Alloy.Globals.menuVisible = true;
		var animation = Titanium.UI.createAnimation();
		animation.right = '40%';
		animation.left = '-40%';
		animation.duration = 150;
		that.parentView.animate(animation);
	}
});
