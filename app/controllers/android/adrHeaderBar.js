var args = arguments[0] || {};
var that = this;
this.parentView = args.parentView;
$.lblTitle.text = args.title;

// eventListener to change headerBarTitle
Ti.App.addEventListener('setHeaderBarTitle', function(e) {
	$.lblTitle.text = e.headerBarTitle;
});

if (args.isFlyout) {
	$.menuButton.visible = true;
	$.menuButton.enabled = true;
	$.backButton.visible = false;
	$.backButton.enabled = false;
	//
	$.btnView.addEventListener('click', function(e) {
		if (Alloy.Globals.menuVisible) {
			Alloy.Globals.menuVisible = false;
			var animation = Titanium.UI.createAnimation();
			animation.right = 0;
			animation.left = 0;
			animation.duration = 150;
			that.parentView.animate(animation);
			Ti.App.fireEvent('hideFlyoutTable');
		} else {
			Alloy.Globals.menuVisible = true;
			Ti.App.fireEvent('showFlyoutTable');
			var animation = Titanium.UI.createAnimation();
			animation.right = '-60%';
			animation.left = '60%';
			animation.duration = 150;
			that.parentView.animate(animation);
		}
	});
} else {
	$.backButton.visible = true;
	$.backButton.enabled = true;
	$.menuButton.visible = false;
	$.menuButton.enabled = false;
	//
	$.backButton.addEventListener('click', function(e) {
		that.parentView.close();
	});
}

