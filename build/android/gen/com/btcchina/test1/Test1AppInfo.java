package com.btcchina.test1;

import org.appcelerator.titanium.ITiAppInfo;
import org.appcelerator.titanium.TiApplication;
import org.appcelerator.titanium.TiProperties;
import org.appcelerator.kroll.common.Log;

/* GENERATED CODE
 * Warning - this class was generated from your application's tiapp.xml
 * Any changes you make here will be overwritten
 */
public final class Test1AppInfo implements ITiAppInfo
{
	private static final String LCAT = "AppInfo";

	public Test1AppInfo(TiApplication app) {
	}

	public String getDeployType() {
		return "development";
	}

	public String getId() {
		return "com.btcchina.test1";
	}

	public String getName() {
		return "test1";
	}

	public String getVersion() {
		return "1.0";
	}

	public String getPublisher() {
		return "eight";
	}

	public String getUrl() {
		return "http://btcchina.com";
	}

	public String getCopyright() {
		return "2014 by eight";
	}

	public String getDescription() {
		return "";
	}

	public String getIcon() {
		return "appicon.png";
	}

	public boolean isAnalyticsEnabled() {
		return false;
	}

	public String getGUID() {
		return "e4378db0-210b-4302-9dd5-16a5907f1b84";
	}

	public boolean isFullscreen() {
		return false;
	}

	public String getBuildType() {
		return "";
	}
}
